<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'order', 'title', 'image', 'note', 'in_trash', 'finished',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'int',
        'user_id'     => 'int',
        'category_id' => 'int',
        'order'       => 'int',
        'title'       => 'string',
        'image'       => 'string',
        'note'        => 'string',
        'in_trash'    => 'boolean',
        'finished'    => 'boolean',
    ];

    /**
     * Get the user that owns the item.
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the category the item belongs to.
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function Category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the checked attribution for finished control checkbox.
     *
     * @return string
     */
    public function get_checked()
    {
        return $this->finished ? 'checked' : '';
    }
}
