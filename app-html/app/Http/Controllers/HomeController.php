<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trash_box = false;
        return view('home', compact('trash_box'));
    }

    /**
     * Display a listing of the resource in trash box.
     *
     * @return \Illuminate\Http\Response
     */

    public function trashBox()
    {
        $trash_box = true;
        return view('home', compact('trash_box'));
    }

    /**
     * Get the total number of items.
     *
     * @return json
     */
    public function getCount()
    {
        return response()->json([
            'finished' => Auth::user()->itemsCount('finished',100),
            'unfinished' => Auth::user()->itemsCount('unfinished',100),
            'total' => Auth::user()->itemsCount('total'),
        ]);
    }
}
