# Bucket List

Web application with a To Do List function, created by building a Laravel environment on Docker.

## Demo
https://bucket-list-laravel.herokuapp.com/

* This application is published by Heroku's free plan. As such, it will go into sleep mode if there is no access for 30 minutes. **You will need to wait for a certain amount of time** because it will need to be restarted when you access it after that.
* Please log in with **"test@example.com"** as E-mail address and **"password"** as password.

<div align="center">

![Demo Image](readme/demo.png)

</div>

## Requirement

### [Docker](https://www.docker.com/)

- [How to install](https://docs.docker.com/get-docker/)

### [Git](https://git-scm.com/)

- [How to install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### [Nginx server for local develop environment in docker](https://gitlab.com/manabe-engineer/nginx-for-local-env-in-docker)

- [How to install](https://gitlab.com/manabe-engineer/nginx-for-local-env-in-docker#installation)

### [PHP](https://www.php.net/)

- [How to install](https://www.php.net/manual/en/install.php)
- For PHP code formatting

### [Composer](https://getcomposer.org/)

- [How to install](https://getcomposer.org/download/)
- For PHP code formatting
### [Node.js](https://nodejs.org/)

- [How to install](https://nodejs.org/en/download/)
- For JavaScript code formatting

## Installation


```bash
git clone git@gitlab.com:manabe-engineer/bucket-list.git
cd bucket-list
make init
```

## Author
### MANABE yusuke
* E-mail: manabe.engineer@gmail.com
# License

"Bucket List" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
