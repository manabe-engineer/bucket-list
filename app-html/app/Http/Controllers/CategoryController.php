<?php

namespace App\Http\Controllers;

use App\User;
use App\Category;
use Illuminate\Http\Request;
use Validator;
use Auth;

class CategoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('/#modal-Category')
                ->withInput()
                ->withErrors($validator, 'addCategory');
        }

        $category          = new Category;
        $category->title   = $request->title;
        $category->user_id = Auth::user()->id;

        $categories = Auth::user()->categories();
        if ($categories->exists()) {
            $category->order = $categories->max('order') + 1;
        } else {
            $category->order = 1;
        }

        $category->save();

        return redirect('/#category-' . $category->id . '-title');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('/#modal-editCategory-' . $category->id)
                ->withInput()
                ->withErrors($validator, 'editCategory');
        }

        $category->title = $request->title;
        $category->save();
        return redirect('/#category-' . $category->id . '-title');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if (Auth::user()->id != $category->user_id) {
            return redirect('/#modal-editCategory-'. $item->id)->with('error', 'This removed Operation not permitted.');
        }

        $category->items()->update(['category_id' => 0]);
        $category->delete();
        Auth::user()->resortItemOrder();
        return redirect('/')->with('success', 'Removed the target category.');
    }
}
