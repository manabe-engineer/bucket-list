-include .env


## Initialize after cloning this repository -


.PHONY: init


init:
	cp -n .env.example .env
	composer install
	npm i
	npm audit fix
	docker-compose up -d --build
	docker-compose exec php composer install
	docker-compose exec php cp .env.example .env
	docker-compose exec php php artisan key:generate
	docker-compose exec php php artisan storage:link
	docker-compose exec php php artisan migrate:fresh --seed
	docker-compose exec php bash -c "npm i && npm audit fix && npm run dev"


## Docker controll ---------------------


.PHONY: up build down stop destroy rebuild ps


up:
	docker-compose up -d
	@make ps

build:
	docker-compose build

down:
	docker-compose down

stop:
	docker-compose stop

destroy:
	docker-compose down --rmi all --volumes

rebuild:
	@make destroy
	@make build

ps:
	docker-compose ps

## Per-container operations ------------


.PHONY: logs-php bash-php restart-php printenv-php \
	logs-nginx bash-nginx restart-nginx printenv-nginx \
	logs-db bash-db restart-db printenv-db \


logs-php:
	docker-compose logs --tail="10" php

bash-php:
	docker-compose exec php bash

restart-php:
	docker-compose restart php

printenv-php:
	docker-compose exec php printenv


logs-nginx:
	docker-compose logs --tail="10" nginx

bash-nginx:
	docker-compose exec nginx bash

restart-nginx:
	docker-compose restart nginx

printenv-nginx:
	docker-compose exec nginx printenv


logs-db:
	docker-compose logs --tail="10" db

bash-db:
	docker-compose exec db bash

restart-db:
	docker-compose restart db

printenv-db:
	docker-compose exec db printenv


## laravrel Oparations----------------------------


.PHONY: migrate cache-clear


migrate:
	docker-compose exec php php artisan migrate:fresh --seed

cache-clear:
	docker-compose exec php php artisan cache:clear
	docker-compose exec php php artisan config:cache
	docker-compose exec php php artisan route:clear
	docker-compose exec php php artisan view:clear
	docker-compose exec php php artisan clear-compiled
	docker-compose exec php composer dump-autoload


## Heroku operations -------------------

.PHONY: heroku-login heroku-apps \
	heroku-bash-stg heroku-restart-stg heroku-logs-stg \
	heroku-migrate-stg heroku-cache-clear-stg heroku-set-app-key-stg \
	heroku-bash-prd heroku-restart-prd heroku-logs-prd \
	heroku-migrate-prd heroku-cache-clear-prd heroku-set-app-key-prd


heroku-login:
	docker-compose exec php heroku login -i

heroku-apps:
	docker-compose exec php heroku apps


heroku-bash-stg:
	docker-compose exec php heroku run bash -a $(HEROKU_STG_APP_NAME)

heroku-restart-stg:
	docker-compose exec php heroku restart -a $(HEROKU_STG_APP_NAME)

heroku-logs-stg:
		docker-compose exec php heroku logs --tail -a $(HEROKU_STG_APP_NAME)

heroku-migrate-stg:
	docker-compose exec php heroku run php artisan migrate:refresh --seed --force -a $(HEROKU_STG_APP_NAME)

heroku-cache-clear-stg:
	docker-compose exec php heroku run php artisan cache:clear -a $(HEROKU_STG_APP_NAME)
	docker-compose exec php heroku run php artisan config:cache -a $(HEROKU_STG_APP_NAME)
	docker-compose exec php heroku run php artisan route:clear -a $(HEROKU_STG_APP_NAME)
	docker-compose exec php heroku run php artisan cache:clear -a $(HEROKU_STG_APP_NAME)
	docker-compose exec php heroku run php artisan view:clear -a $(HEROKU_STG_APP_NAME)
	docker-compose exec php heroku run php artisan clear-compiled -a $(HEROKU_STG_APP_NAME)
	docker-compose exec php heroku run composer dump-autoload -a $(HEROKU_STG_APP_NAME)

heroku-set-app-key-stg:
	docker-compose exec php heroku config:set APP_KEY=$(php artisan --no-ansi key:generate --show) -a $(HEROKU_STG_APP_NAME)


heroku-bash-prd:
	docker-compose exec php heroku run bash -a $(HEROKU_PRD_APP_NAME)

heroku-restart-prd:
	docker-compose exec php heroku restart -a $(HEROKU_PRD_APP_NAME)

heroku-logs-prd:
		docker-compose exec php heroku logs --tail -a $(HEROKU_PRD_APP_NAME)


heroku-migrate-prd:
	docker-compose exec php heroku run php artisan migrate:refresh --seed --force -a $(HEROKU_PRD_APP_NAME)

heroku-cache-clear-prd:
	docker-compose exec php heroku run php artisan cache:clear -a $(HEROKU_PRD_APP_NAME)
	docker-compose exec php heroku run php artisan config:cache -a $(HEROKU_PRD_APP_NAME)
	docker-compose exec php heroku run php artisan route:clear -a $(HEROKU_PRD_APP_NAME)
	docker-compose exec php heroku run php artisan cache:clear -a $(HEROKU_PRD_APP_NAME)
	docker-compose exec php heroku run php artisan view:clear -a $(HEROKU_PRD_APP_NAME)
	docker-compose exec php heroku run php artisan clear-compiled -a $(HEROKU_PRD_APP_NAME)
	docker-compose exec php heroku run composer dump-autoload -a $(HEROKU_PRD_APP_NAME)

heroku-set-app-key-prd:
	docker-compose exec php heroku config:set APP_KEY=$(php artisan --no-ansi key:generate --show) -a $(HEROKU_PRD_APP_NAME)


## Code formatter ----------------------


.PHONY: phpcs phpcbf eslint stylelint


phpcs:
	./vendor/bin/phpcs --standard=./ruleset.xml ./

phpcbf:
	./vendor/bin/phpcbf --standard=./ruleset.xml ./

eslint:
	npm run eslint

stylelint:
	npm run stylelint
