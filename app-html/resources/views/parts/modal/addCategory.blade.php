<div class="modal fade" id="modal-addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add category</h5>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

            <div class="modal-body">
                <form action="/category/store" id="modal-addCategory-form" method="post">
                    @csrf
@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
    @foreach ($error->addCategory->all() as $error)
                            <li>{{ $error }}</li>
    @endforeach
                    </ul>
                </div>
@endif
                    <div class="form-group row">
                        <label for="add-category-title" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="add-category-title" name="title">
                        </div>
                    </div>

                </form>

            </div>

            <div class="modal-footer">
                <button type="submit" form="modal-addCategory-form" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </div>
</div>
