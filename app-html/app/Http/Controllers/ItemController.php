<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use Illuminate\Http\Request;
use Validator;
use Auth;

class ItemController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|string',
            'note'        => 'nullable|string',
            'category_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect('/#modal-addItem')
                ->withInput()
                ->withErrors($validator, 'addItem');
        }

        $item        = new Item;
        $item->title = $request->title;
        if ($request->note) {
            $item->note = $request->note;
        } else {
            $item->note = '';
        }
        $item->user_id     = Auth::user()->id;
        $item->category_id = $request->category_id;
        $item->in_trash    = false;
        $item->finished    = false;
        $item->image       = '';

        $items = Auth::user()->items();
        if ($items->exists()) {
            $item->order = $items->max('order') + 1;
        } else {
            $item->order = 1;
        }

        $item->save();

        Auth::user()->resortItemOrder();

        return redirect('/#item-' . $item->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|string',
            'note'        => 'nullable|string',
            'category_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect('/#modal-editItem-' . $item->id)
                ->withInput()
                ->withErrors($validator, 'editItem');
        }

        $item->title = $request->title;
        if ($request->note) {
            $item->note = $request->note;
        } else {
            $item->note = '';
        }

        $item->save();

        if ($item->category_id != $request->category_id) {
            $item->category_id = $request->category_id;

            $items = Auth::user()->items();
            if ($items->exists()) {
                $item->order = $items->max('order') + 1;
            } else {
                $item->order = 1;
            }

            $item->save();
            Auth::user()->resortItemOrder();
        }

        return redirect('/#item-' . $item->id);
    }

    /**
     * Change Finished state.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return json
     */
    public function changeFinishedState(Request $request, Item $item)
    {
        $validator = Validator::make($request->all(), [
            'finished' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'states'  => 'failure',
                'message' => 'State Change is Failure. A Validation error occurred.',
                'errors' => $request->finished,
            ]);
        }

        $item->finished = $request->finished;
        $item->save();

        if($item->finished){
            $massage = 'This item changes to finished state.';
        }else{
            $massage = 'This item changes to unfinished state.';
        }

        return response()->json([
            'states'  => 'success',
            'message' => $massage,
        ]);
    }

    /**
     * Move the item to the trash box.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function intoTrash(Item $item)
    {
        $item->in_trash = true;

        $item->save();
        Auth::user()->resortItemOrder();
        return redirect('/');
    }


    /**
     * Return the item from the trash box.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function backFromTrash(Item $item)
    {
        $item->in_trash = false;

        $item->save();
        Auth::user()->resortItemOrder();
        return redirect('/trash');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if (Auth::user()->id != $item->user_id) {
            return redirect('/trash#modal-editItem-'. $item->id )->with('error', 'This removed Operation not permitted.');
        }

        $item->delete();
        return redirect('/trash')->with('success', 'Removed the target item.');
    }
}
