<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

$factory->define(Item::class, function (Faker $faker) {
    $output = [
        'title' => $faker->sentence,
        'note' => $faker->text,
        'in_trash' => $faker->boolean(40),
        'finished' => $faker->boolean(40),
        'category_id' => 0,
        'order' => 0,
        'image' => '',
    ];



    if ($faker->boolean(0)) {
        if ( ! file_exists (storage_path('app/public/images/')) ){
            mkdir(storage_path('app/public/images/'), 0755);
        }
        $url = 'https://picsum.photos/300/200';
        $file_name = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true)) . '.jpg';
        $file_path = storage_path('app/public/images/' . $file_name);

        Image::make($url)->save($file_path);
        $output['image'] = 'images/' . $file_name;
    }

    return $output;
});
