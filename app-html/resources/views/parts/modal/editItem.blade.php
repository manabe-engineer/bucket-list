<div class="modal fade" id="modal-editItem-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Edit item</h5>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

            <div class="modal-body">
                <form action="/update/{{ $item->id }}" id="modal-editItem-{{ $item->id }}-form" method="post">
                    @csrf
                    @method('patch')
@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
    @foreach ($error->editItem->all() as $error)
                            <li>{{ $error }}</li>
    @endforeach
                    </ul>
                </div>
@endif
                    <div class="form-group row">
                        <label for="modal-editItem-{{ $item->id }}-title" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="modal-editItem-{{ $item->id }}-title" name="title" value="{{ $item->title }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="modal-editItem-{{ $item->id }}-note" class="col-sm-4 col-form-note">Note</label>
                        <div class="col-sm-8">
                            <textarea row="4" class="form-control" id="modal-editItem-{{ $item->id }}-note" name="note">{{ $item->note }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="modal-editItem-{{ $item->id }}-category" class="col-sm-4 col-form-category">Category</label>
                        <div class="col-sm-8">
                            <select name="category_id" class="form-control" id="modal-editItem-{{ $item->id }}-category">
                                <option value="0" @if( $item->category_id === 0 ) selected @endif>Uncategorized</option>
@foreach (Auth::user()->categories as $category)
                                <option value="{{ $category->id }}" @if( $item->category_id === $category->id ) selected @endif>{{ $category->title }}</option>
@endforeach
                            </select>
                        </div>
                    </div>

                </form>

@if (!$trash_box)
                <form action="/into-trash/{{ $item->id }}" id="intoTrash-{{ $item->id }}-form" method="POST" >
                    @csrf
                    @method('patch')
                </form>
@else
                <form action="/delete/{{ $item->id }}" id="deleteItem-{{ $item->id }}-form" method="POST")">
                    @csrf
                    @method('delete')
                </form>
                <form action="/back-from-trash/{{ $item->id }}" id="backFromTrash-{{ $item->id }}-form" method="POST" >
                    @csrf
                    @method('patch')
                </form>
@endif
            </div>

            <div class="modal-footer">
@if (!$trash_box)
                <button type="submit" form="intoTrash-{{ $item->id }}-form" class="btn btn-warning">Into the trash box</button>
@else
                <button type="submit" form="deleteItem-{{ $item->id }}-form" class="btn btn-danger">Delete</button>
                <button type="submit" form="backFromTrash-{{ $item->id }}-form" class="btn btn-warning">Back from the trash box</button>
@endif
                <button type="submit" form="modal-editItem-{{ $item->id }}-form" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </div>
</div>
