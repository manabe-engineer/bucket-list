document.addEventListener('DOMContentLoaded', function() {
  const hash = window.location.hash
  const pattern = '#modal-'
  if (hash.indexOf(pattern) === 0) {
    document.getElementById(hash.slice(1)).modal('toggle')
  }
})
