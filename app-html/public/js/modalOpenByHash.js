/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*****************************************!*\
  !*** ./resources/js/modalOpenByHash.js ***!
  \*****************************************/
document.addEventListener('DOMContentLoaded', function () {
  var hash = window.location.hash;
  var pattern = '#modal-';

  if (hash.indexOf(pattern) === 0) {
    document.getElementById(hash.slice(1)).modal('toggle');
  }
});
/******/ })()
;